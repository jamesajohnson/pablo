window.addEventListener("load", function load (event) {
    window.removeEventListener("load", load, false);
    
    addClickEventsToMenuToggleButtons();
}, false);

function addClickEventsToMenuToggleButtons() {
    var menuToggleButtons = document.querySelectorAll('.menu-toggle');
    
    var i;
    for (i = 0; i < menuToggleButtons.length; i ++) {
        menuToggleButtons[i].addEventListener('click', function () {
            var element = document.getElementById(this.getAttribute('for'));
            hideAllMenus(this.getAttribute('for'));
            toggleClass(element, "visible");
        });
    }
}

function hideAllMenus (except = '') {
    var menus = document.querySelectorAll('.menu-content.visible');

    var i = 0;
    for (i = 0; i< menus.length; i++) {
        if (menus[i].getAttribute("id") !== except) {
            toggleClass(menus[i], "visible");
        }
    }
}

function toggleClass (element, className) {    
    if (element.classList) { 
        element.classList.toggle(className);
    } else {
        // For IE9
        var classes = element.className.split(" ");
        var i = classes.indexOf(className);
        
        if (i >= 0) {
            classes.splice(i, 1);
        } else {
            classes.push(className);
            element.className = classes.join(" "); 
        }
    }
}