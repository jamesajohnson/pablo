// Gulp.js configuration
var
    // modules
    gulp = require('gulp'),
    gls = require('gulp-live-server'),
    htmlclean = require('gulp-htmlclean'),
    sass = require('gulp-sass'),
    newer = require('gulp-newer'),
    postcss = require('gulp-postcss'),
    assets = require('postcss-assets'),
    autoprefixer = require('autoprefixer'),
    mqpacker = require('css-mqpacker'),
    cssnano = require('cssnano'),
    concat = require('gulp-concat'),
    deporder = require('gulp-deporder'),
    stripdebug = require('gulp-strip-debug'),
    uglify = require('gulp-uglify'),

    // development mode?
    devBuild = (process.env.NODE_ENV !== 'production'),

    // folders
    folder = {
        src: 'src/',
        build: 'build/'
    };

// HTML processing
gulp.task('html', function () {
    var
        out = folder.build,
        page = gulp.src(folder.src + 'html/**/*')
            .pipe(newer(out));

    // minify production code
    if (!devBuild) {
        page = page.pipe(htmlclean());
    }

    return page.pipe(gulp.dest(out));
});

// JavaScript processing
gulp.task('js', function () {
    var jsbuild = gulp.src(folder.src + 'js/**/*')
        .pipe(deporder())
        .pipe(concat('main.js'));

    if (!devBuild) {
        jsbuild = jsbuild
            .pipe(stripdebug())
            .pipe(uglify());
    }

    return jsbuild.pipe(gulp.dest(folder.build + 'js/'));

});

// CSS processing
gulp.task('css', function () {

    var postCssOpts = [
        assets({ loadPaths: ['images/'] }),
        autoprefixer({ browsers: ['last 2 versions', '> 2%'] }),
        mqpacker
    ];

    if (!devBuild) {
        postCssOpts.push(cssnano);
    }

    return gulp.src(folder.src + 'scss/main.scss')
        .pipe(sass({
            outputStyle: 'nested',
            imagePath: 'images/',
            precision: 3,
            errLogToConsole: true
        }))
        .pipe(postcss(postCssOpts))
        .pipe(gulp.dest(folder.build + 'css/'));

});

// run all tasks
gulp.task('run', ['html', 'css', 'js']);

// watch for changes
gulp.task('watch', function () {

    // image changes
    gulp.watch(folder.src + 'images/**/*', ['images']);

    // html changes
    gulp.watch(folder.src + 'html/**/*', ['html']);

    // javascript changes
    gulp.watch(folder.src + 'js/**/*', ['js']);

    // css changes
    gulp.watch(folder.src + 'scss/**/*', ['css']);

});

gulp.task('serve', function () {
    //1. serve with default settings 
    var server = gls.static(); //equals to gls.static('public', 3000); 
    server.start();

    //2. serve at custom port 
    var server = gls.static('dist', 8888);
    server.start();

    //3. serve multi folders 
    var server = gls.static(['dist', '.tmp']);
    server.start();

    //use gulp.watch to trigger server actions(notify, start or stop) 
    gulp.watch(['static/**/*.css', 'static/**/*.html'], function (file) {
        server.notify.apply(server, [file]);
    });
});

// default task
gulp.task('default', ['run', 'watch']);